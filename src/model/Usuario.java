package model;

public class Usuario {
    private String nombresYApellidos;
    private String documento;
    private String telefono;
    private String usuario;
    private String contraseña;
    private String email;
    private String edad;
    private String genero;
    private String preguntaSeguridad;
    private String respuestaSeguridad;

    public Usuario(String nombresYApellidos, String documento, String telefono, String usuario, String contraseña, String email, String edad, String genero, String preguntaSeguridad,String respuestaSeguridad) {
        this.nombresYApellidos = nombresYApellidos;
        this.documento = documento;
        this.telefono = telefono;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.email = email;
        this.edad = edad;
        this.genero = genero;
        this.preguntaSeguridad = preguntaSeguridad;
        this.respuestaSeguridad = respuestaSeguridad;
    }

    public String getPreguntaSeguridad() {
        return preguntaSeguridad;
    }

    public void setPreguntaSeguridad(String preguntaSeguridad) {
        this.preguntaSeguridad = preguntaSeguridad;
    }

    public String getNombresYApellidos() {
        return nombresYApellidos;
    }

    public void setNombresYApellidos(String nombresYApellidos) {
        this.nombresYApellidos = nombresYApellidos;
    }

    public String getRespuestaSeguridad() {
        return respuestaSeguridad;
    }

    public void setRespuestaSeguridad(String respuestaSeguridad) {
        this.respuestaSeguridad = respuestaSeguridad;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    @Override
    public Usuario clone() throws CloneNotSupportedException {
        return (Usuario) super.clone();
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nombresYApellidos='" + nombresYApellidos + '\'' +
                ", documento='" + documento + '\'' +
                ", telefono='" + telefono + '\'' +
                ", usuario='" + usuario + '\'' +
                ", contraseña='" + contraseña + '\'' +
                ", email='" + email + '\'' +
                ", edad='" + edad + '\'' +
                ", genero='" + genero + '\'' +
                ", preguntaSeguridad='" + preguntaSeguridad + '\'' +
                ", respuestaSeguridad='" + respuestaSeguridad + '\'' +
                '}';
    }
}
