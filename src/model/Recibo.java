package model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Recibo {
    private static List<Servicio> serviciosRealizados;
    private int costoTotal;
    private LocalDateTime fechaRecibo;
    private Empleado empleadoEncargado;

    public Recibo(int costoTotal, LocalDateTime fechaRecibo, Empleado empleadoEncargado) {
        this.costoTotal = costoTotal;
        this.fechaRecibo = fechaRecibo;
        this.empleadoEncargado = empleadoEncargado;
    }

    public static List<Servicio> getServiciosRealizados() {
        return serviciosRealizados;
    }

    public static void setServiciosRealizados(List<Servicio> serviciosRealizados) {
        Recibo.serviciosRealizados = serviciosRealizados;
    }

    public int getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(int costoTotal) {
        this.costoTotal = costoTotal;
    }

    public LocalDateTime getFechaRecibo() {
        return fechaRecibo;
    }

    public void setFechaRecibo(LocalDateTime fechaRecibo) {
        this.fechaRecibo = fechaRecibo;
    }

    public Empleado getEmpleadoEncargado() {
        return empleadoEncargado;
    }

    public void setEmpleadoEncargado(Empleado empleadoEncargado) {
        this.empleadoEncargado = empleadoEncargado;
    }

    @Override
    public String toString() {
        return "Recibo{" +
                "costoTotal=" + costoTotal +
                ", fechaRecibo=" + fechaRecibo +
                ", empleadoEncargado=" + empleadoEncargado +
                '}';
    }
}
