package model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Cita {
    private Cliente clienteSolicitante;
    private String fecha;
    private String hora;
    private Servicio servicioRequerido;

    public Cita(Cliente clienteSolicitante, String fecha, String hora, Servicio servicioRequerido) {
        this.clienteSolicitante = clienteSolicitante;
        this.fecha = fecha;
        this.hora = hora;
        this.servicioRequerido = servicioRequerido;
    }

    public Cliente getClienteSolicitante() {
        return clienteSolicitante;
    }

    public void setClienteSolicitante(Cliente clienteSolicitante) {
        this.clienteSolicitante = clienteSolicitante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Servicio getServicioRequerido() {
        return servicioRequerido;
    }

    public void setServicioRequerido(Servicio servicioRequerido) {
        this.servicioRequerido = servicioRequerido;
    }

    @Override
    public String toString() {
        return "Cita{" +
                "clienteSolicitante=" + clienteSolicitante +
                ", fecha='" + fecha + '\'' +
                ", hora='" + hora + '\'' +
                ", servicioRequerido=" + servicioRequerido +
                '}';
    }
}
