package model;

import java.util.ArrayList;
import java.util.List;

public class Empleado extends Usuario{

    private String salario;
    private String cargo;
    private static List<Cliente> clientesAtendidos;

    public Empleado(String nombresYApellidos, String documento, String telefono, String usuario, String contraseña, String email, String edad, String genero, String preguntaSeguridad, String respuestaSeguridad, String salario, String cargo) {
        super(nombresYApellidos, documento, telefono, usuario, contraseña, email, edad, genero, preguntaSeguridad, respuestaSeguridad);
        this.salario = salario;
        this.cargo = cargo;
    }

    public String getSalario() {
        return salario;
    }

    public void setSalario(String salario) {
        this.salario = salario;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public static List<Cliente> getClientesAtendidos() {
        return clientesAtendidos;
    }

    public static void setClientesAtendidos(List<Cliente> clientesAtendidos) {
        Empleado.clientesAtendidos = clientesAtendidos;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "salario=" + salario +
                ", cargo='" + cargo + '\'' +
                '}';
    }
}
