package model;

public class Cliente extends Usuario{
    public Cliente(String nombresYApellidos, String documento, String telefono, String usuario, String contraseña, String email, String edad, String genero, String preguntaSeguridad, String respuestaSeguridad) {
        super(nombresYApellidos, documento, telefono, usuario, contraseña, email, edad, genero, preguntaSeguridad, respuestaSeguridad);
    }
}
