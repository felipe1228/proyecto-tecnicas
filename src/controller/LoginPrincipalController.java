package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import model.Cliente;
import model.Empleado;
import model.Usuario;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardOpenOption.*;

public class LoginPrincipalController {
    @FXML
    private BorderPane contenedorLoginPrincipal;
    @FXML
    private TextField txtUsuario;
    @FXML
    private TextField txtContraseña;

    private final static String NOMBRE_ARCHIVO = "usuario-logueado";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public void btnLogin_action() throws IOException {
        if (Files.exists(ARCHIVO)) {
            Files.delete(ARCHIVO);
            Files.createFile(ARCHIVO);
        }
        else{
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        String usuario = txtUsuario.getText();
        String contraseña = txtContraseña.getText();
        String clienteLogueado = "no";
        String empleadoLogueado = "no";
        String administradorLogueado = "no";

        if(usuario.isEmpty()){
            mostrarMensaje(Alert.AlertType.ERROR, "Login", "Resultado de la transacción", "Por favor ingrese su usuario...");
            txtUsuario.requestFocus();
            return;
        }
        if(contraseña.isEmpty()){
            mostrarMensaje(Alert.AlertType.ERROR, "Login", "Resultado de la transacción", "Por favor ingrese su contraseña...");
            txtContraseña.requestFocus();
            return;
        }
        BufferedReader daoClientes = new BufferedReader(new FileReader("clientes-v2"));
        try {
            String line;
            Cliente cliente;
            while ((line = daoClientes.readLine()) != null) {
                cliente = parseClienteObject(line);
                if(usuario.equals(cliente.getUsuario()) && contraseña.equals(cliente.getContraseña())){
                    clienteLogueado  = "si";
                    Cliente usuarioLogueado = new Cliente(cliente.getNombresYApellidos(),cliente.getDocumento(),cliente.getTelefono(),cliente.getUsuario(),cliente.getContraseña(),cliente.getEmail(),cliente.getEdad(),cliente.getGenero(),cliente.getPreguntaSeguridad(),cliente.getRespuestaSeguridad());
                    String clienteRegistrado = parseClienteString(usuarioLogueado);
                    // Se extraen los bytes del registro
                    byte[] datosCliente = clienteRegistrado.getBytes();
                    // Se ponen los bytes en un buffer
                    ByteBuffer byteBufferCliente = ByteBuffer.wrap(datosCliente);
                    // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
                    try (FileChannel channelCliente = (FileChannel.open(ARCHIVO, WRITE))) {
                        // se escriben los datos del buffer
                        channelCliente.write(byteBufferCliente);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            daoClientes.close();
        }
        BufferedReader daoEmpleados = new BufferedReader(new FileReader("empleados-v2"));
        try {
            String lineEmpleados;
            Empleado empleado;
            while ((lineEmpleados = daoEmpleados.readLine()) != null) {
                empleado = parseEmpleadoObject(lineEmpleados);
                if(usuario.equals(empleado.getUsuario()) && contraseña.equals(empleado.getContraseña())){
                    if(empleado.getCargo().equals("administrador")){
                        administradorLogueado = "si";
                        Empleado usuarioLogueado = new Empleado(empleado.getNombresYApellidos(),empleado.getDocumento(),empleado.getTelefono(),empleado.getUsuario(),empleado.getContraseña(),empleado.getEmail(),empleado.getEdad(),empleado.getGenero(),empleado.getPreguntaSeguridad(),empleado.getRespuestaSeguridad(),empleado.getSalario(),empleado.getCargo());
                        String registroEmpleadoAdmin = parseEmpleadoString(usuarioLogueado);
                        // Se extraen los bytes del registro
                        byte[] datosRegistroEmpleadoAdmin = registroEmpleadoAdmin.getBytes();
                        // Se ponen los bytes en un buffer
                        ByteBuffer byteBufferEmpleadoAdmin = ByteBuffer.wrap(datosRegistroEmpleadoAdmin);
                        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
                        try (FileChannel channelEmpleadoAdmin = (FileChannel.open(ARCHIVO, WRITE))) {
                            // se escriben los datos del buffer
                            channelEmpleadoAdmin.write(byteBufferEmpleadoAdmin);
                            channelEmpleadoAdmin.close();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                    else{
                        empleadoLogueado  = "si";
                        Empleado usuarioLogueado = new Empleado(empleado.getNombresYApellidos(),empleado.getDocumento(),empleado.getTelefono(),empleado.getUsuario(),empleado.getContraseña(),empleado.getEmail(),empleado.getEdad(),empleado.getGenero(),empleado.getPreguntaSeguridad(),empleado.getRespuestaSeguridad(),empleado.getSalario(),empleado.getCargo());
                        String registroEmpleado = parseEmpleadoString(usuarioLogueado);
                        // Se extraen los bytes del registro
                        byte[] datosRegistroEmpleado = registroEmpleado.getBytes();
                        // Se ponen los bytes en un buffer
                        ByteBuffer byteBufferEmpleado = ByteBuffer.wrap(datosRegistroEmpleado);
                        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
                        try (FileChannel channelEmpleado = (FileChannel.open(ARCHIVO, WRITE))) {
                            // se escriben los datos del buffer
                            channelEmpleado.write(byteBufferEmpleado);
                            channelEmpleado.close();
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            daoClientes.close();
        }
        if(clienteLogueado.equals("si")){
            mostrarMensaje(Alert.AlertType.INFORMATION, "Login", "Resultado de la transacción", "Usted se ha logueado con éxito!");
            Parent panelCliente = FXMLLoader.load(getClass().getResource("../view/panel-clientes.fxml"));
            this.contenedorLoginPrincipal.setCenter(panelCliente);
        }
        else if(empleadoLogueado.equals("si")){
            mostrarMensaje(Alert.AlertType.INFORMATION, "Login", "Resultado de la transacción", "Usted se ha logueado con éxito!");
            Parent panelEmpleado = FXMLLoader.load(getClass().getResource("../view/panel-empleado.fxml"));
            this.contenedorLoginPrincipal.setCenter(panelEmpleado);
        }
        else if(administradorLogueado.equals("si")){
            mostrarMensaje(Alert.AlertType.INFORMATION, "Login", "Resultado de la transacción", "Usted se ha logueado con éxito!");
            Parent panelAdministrador = FXMLLoader.load(getClass().getResource("../view/administrador-panel.fxml"));
            this.contenedorLoginPrincipal.setCenter(panelAdministrador);
        }
        else{
            mostrarMensaje(Alert.AlertType.ERROR, "Login", "Resultado de la transacción", "Usuario no encontrado, revise sus credenciales...");
        }

    }

    private Empleado parseEmpleadoObject(String empleadoString) {
        String[] datosEmpleado = empleadoString.split(FIELD_SEPARATOR);
        Empleado empleado = new Empleado(datosEmpleado[0], datosEmpleado[1], datosEmpleado[2], datosEmpleado[3], datosEmpleado[4], datosEmpleado[5],datosEmpleado[6],datosEmpleado[7],datosEmpleado[8],datosEmpleado[9],datosEmpleado[10],datosEmpleado[11]);
        return empleado;
    }


    public void btnCrearUnaNuevaCuenta_action() throws IOException {
        Parent ventanaCrearUnaNuevaCuenta = FXMLLoader.load(getClass().getResource("../view/registrar-cuenta.fxml"));
        this.contenedorLoginPrincipal.setCenter(ventanaCrearUnaNuevaCuenta);
    }

    public void btnOlvideMiContraseña_action() throws IOException {
        Parent ventanaCrearOlvideMiContraseña = FXMLLoader.load(getClass().getResource("../view/olvide-mi-contrasenia.fxml"));
        this.contenedorLoginPrincipal.setCenter(ventanaCrearOlvideMiContraseña);
    }
    private Cliente parseClienteObject(String clienteString) {
        String[] datosCliente = clienteString.split(FIELD_SEPARATOR);
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1], datosCliente[2], datosCliente[3], datosCliente[4], datosCliente[5],datosCliente[6],datosCliente[7],datosCliente[8],datosCliente[9]);
        return cliente;
    }
    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
    private String parseEmpleadoString(Empleado empleado) {
        StringBuilder sb = new StringBuilder();
        sb.append(empleado.getNombresYApellidos()).append(FIELD_SEPARATOR)
                .append(empleado.getDocumento()).append(FIELD_SEPARATOR)
                .append(empleado.getTelefono()).append(FIELD_SEPARATOR)
                .append(empleado.getUsuario()).append(FIELD_SEPARATOR)
                .append(empleado.getContraseña()).append(FIELD_SEPARATOR)
                .append(empleado.getEmail()).append(FIELD_SEPARATOR)
                .append(empleado.getEdad()).append(FIELD_SEPARATOR)
                .append(empleado.getGenero()).append(FIELD_SEPARATOR)
                .append(empleado.getPreguntaSeguridad()).append(FIELD_SEPARATOR)
                .append(empleado.getRespuestaSeguridad()).append(FIELD_SEPARATOR)
                .append(empleado.getSalario()).append(FIELD_SEPARATOR)
                .append(empleado.getCargo()).append(RECORD_SEPARATOR);
        return sb.toString();
    }
    private String parseClienteString(Cliente cliente) {
        StringBuilder sb = new StringBuilder();
        sb.append(cliente.getNombresYApellidos()).append(FIELD_SEPARATOR)
                .append(cliente.getDocumento()).append(FIELD_SEPARATOR)
                .append(cliente.getTelefono()).append(FIELD_SEPARATOR)
                .append(cliente.getUsuario()).append(FIELD_SEPARATOR)
                .append(cliente.getContraseña()).append(FIELD_SEPARATOR)
                .append(cliente.getEmail()).append(FIELD_SEPARATOR)
                .append(cliente.getEdad()).append(FIELD_SEPARATOR)
                .append(cliente.getGenero()).append(FIELD_SEPARATOR)
                .append(cliente.getPreguntaSeguridad()).append(FIELD_SEPARATOR)
                .append(cliente.getRespuestaSeguridad()).append(RECORD_SEPARATOR);
        return sb.toString();
    }
}
