package controller;

import bsn.ServicioBsn;
import bsn.exception.ServicioYaExisteException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import model.Servicio;

import java.io.IOException;

public class AgregarNuevoServicioController {
    @FXML
    private BorderPane contenedorAgregarNuevoServicio;
    @FXML
    private TextField txtNombreServicio;
    @FXML
    private TextField txtDescripcionServicio;
    @FXML
    private TextField txtPrecioServicio;
    // conexión con el negocio

    private ServicioBsn servicioBsn;

    public AgregarNuevoServicioController() {
        this.servicioBsn = new ServicioBsn();
    }
    //todo actualizar tabla de servicios


    public void btnRegresar_action() throws IOException {
        Parent ventanaModificarServicios = FXMLLoader.load(getClass().getResource("../view/modificar-servicios.fxml"));
        this.contenedorAgregarNuevoServicio.setCenter(ventanaModificarServicios);
    }

    public void btnGuardar_action() {
        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String nombre = txtNombreServicio.getText().trim();
        String descripcion = txtDescripcionServicio.getText().trim();
        String precio = txtPrecioServicio.getText().trim();


        // se valida que los nombre contenga un valor
        if (nombre.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El nombre del servicio es requerido");
            txtNombreServicio.requestFocus();
            return;
        }


        // se valida que la descripcion del servicio contenga un valor
        if (descripcion.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La descripción del servicio es requerida");
            txtDescripcionServicio.requestFocus();
            return;
        }
        // se valida que el precio contenga un valor
        if (precio.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El precio del servicio es requerido");
            txtPrecioServicio.requestFocus();
            return;
        }

        Servicio servicio = new Servicio(nombre, descripcion, precio);
        try {
            //todo hacer que los métodos de almacenamiento retornen el objeto almacenado (copia)
            this.servicioBsn.registrarServicio(servicio);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro", "Resultado de la transacción", "Se ha sido registrado con éxito");
            limpiarCampos();
            //todo clonar el cliente y agregarlo a la tabla de clientes
            //...
        } catch (ServicioYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", e.getMessage());
        }
        //todo tirar excepcion de clonacion no soportada y refrescar tabla
        // catch (CloneNotSupportedException cnse){
        //    this.refrescarTabla();
        //}
    }
    private void limpiarCampos() {
        this.txtNombreServicio.clear();
        this.txtDescripcionServicio.clear();
        this.txtPrecioServicio.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
}
