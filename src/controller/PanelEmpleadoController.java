package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class PanelEmpleadoController {
    @FXML
    private BorderPane contenedorPanelEmpleado;

    public void btnCitas_action() throws IOException {
        Parent citasEmpleado = FXMLLoader.load(getClass().getResource("../view/panel-citas-empleado.fxml"));
        this.contenedorPanelEmpleado.setCenter(citasEmpleado);
    }

    public void btnClientes_action() throws IOException {
        Parent clientesEmpleado = FXMLLoader.load(getClass().getResource("../view/panel-clientes-empleado.fxml"));
        this.contenedorPanelEmpleado.setCenter(clientesEmpleado);
    }

    public void btnRegresar_action() throws IOException {
        Parent loginPrincipal = FXMLLoader.load(getClass().getResource("../view/login-principal.fxml"));
        this.contenedorPanelEmpleado.setCenter(loginPrincipal);
    }
}
