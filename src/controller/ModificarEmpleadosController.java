package controller;

import bsn.EmpleadoBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import model.Empleado;

import java.io.IOException;
import java.util.List;

public class ModificarEmpleadosController {
    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();
    // conexión con el negocio
    private EmpleadoBsn empleadoBsn;

    public ModificarEmpleadosController() {
        this.empleadoBsn = new EmpleadoBsn();
    }
    @FXML
    private BorderPane contenedorModificarEmpleados;
    @FXML
    private TableView<Empleado> tblEmpleados;
    @FXML
    private TableColumn<Empleado, String> clmNombres;
    @FXML
    private TableColumn<Empleado, String> clmDocumento;
    @FXML
    private TableColumn<Empleado, String> clmTelefono;
    @FXML
    private TableColumn<Empleado, String> clmUsuario;
    @FXML
    private TableColumn<Empleado, String> clmContraseña;
    @FXML
    private TableColumn<Empleado, String> clmEmail;
    @FXML
    private TableColumn<Empleado, String> clmEdad;
    @FXML
    private TableColumn<Empleado, String> clmGenero;
    @FXML
    private TableColumn<Empleado, String> clmPreguntaSeguridad;
    @FXML
    private TableColumn<Empleado, String> clmRespuestaSeguridad;
    @FXML
    private TableColumn<Empleado, String> clmCargo;
    @FXML
    private TableColumn<Empleado, String> clmSalario;
    @FXML
    public void initialize(){
        clmNombres.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombresYApellidos()));
        clmDocumento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDocumento()));
        clmTelefono.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTelefono()));
        clmUsuario.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUsuario()));
        clmContraseña.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getContraseña()));
        clmEmail.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEmail()));
        clmEdad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEdad()));
        clmGenero.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getGenero()));
        clmPreguntaSeguridad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPreguntaSeguridad()));
        clmRespuestaSeguridad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getRespuestaSeguridad()));
        clmCargo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCargo()));
        clmSalario.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getSalario()));
        refrescarTabla();
    }
    public void btnRegresar_action() throws IOException {
        Parent ventanaAdministradorPanel = FXMLLoader.load(getClass().getResource("../view/administrador-panel.fxml"));
        this.contenedorModificarEmpleados.setCenter(ventanaAdministradorPanel);
    }

    public void btnAgregarNuevoEmpleado_action() throws IOException {
        Parent agregarNuevoEmpleado = FXMLLoader.load(getClass().getResource("../view/agregar-nuevo-empleado.fxml"));
        this.contenedorModificarEmpleados.setCenter(agregarNuevoEmpleado);
    }
    private void refrescarTabla(){
        List<Empleado> empleadosList = empleadoBsn.consultarEmpleados();
        ObservableList<Empleado> empleadosListObservable = FXCollections.observableList(empleadosList);
        tblEmpleados.setItems(empleadosListObservable);
    }
}
