package controller;

import bsn.CitaBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import model.Cita;

import java.io.IOException;
import java.util.List;

public class PanelCitasEmpleadoController {
    @FXML
    private BorderPane contenedorPanelCitasEmpleado;
    @FXML
    private TableView<Cita> tblCitas;
    @FXML
    private TableColumn<Cita, String> clmCliente;
    @FXML
    private TableColumn<Cita, String> clmFecha;
    @FXML
    private TableColumn<Cita, String> clmHora;
    @FXML
    private TableColumn<Cita, String> clmProcedimiento;
    @FXML
    private TableColumn<Cita, String> clmPrecio;

    // conexión con el negocio
    private CitaBsn citaBsn;

    public PanelCitasEmpleadoController() {
        this.citaBsn = new CitaBsn();
    }

    @FXML
    public void initialize(){
        clmCliente.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClienteSolicitante().getUsuario()));
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha()));
        clmHora.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHora()));
        clmProcedimiento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getServicioRequerido().getNombre()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getServicioRequerido().getPrecio()));
        refrescarTabla();
    }


    public void btnRegresar_action() throws IOException {
        Parent panelEmpleado = FXMLLoader.load(getClass().getResource("../view/panel-empleado.fxml"));
        this.contenedorPanelCitasEmpleado.setCenter(panelEmpleado);
    }
    private void refrescarTabla(){
        List<Cita> citasList = citaBsn.consultarCitas();
        ObservableList<Cita> citasListObservable = FXCollections.observableList(citasList);
        tblCitas.setItems(citasListObservable);
    }
}
