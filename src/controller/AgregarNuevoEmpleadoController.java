package controller;

import bsn.EmpleadoBsn;
import bsn.exception.EmpleadoYaExisteException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

import model.Empleado;

import java.io.IOException;

public class AgregarNuevoEmpleadoController {
    @FXML
    private BorderPane contenedorAgregarNuevoEmpleado;
    @FXML
    private TextField txtNombresYApellidos;
    @FXML
    private TextField txtDocumento;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField txtContraseña;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtEdad;
    @FXML
    private ChoiceBox txtGenero;
    @FXML
    private ChoiceBox txtPreguntaSeguridad;
    @FXML
    private TextField txtRespuestaSeguridad;
    @FXML
    private TextField txtSalario;
    @FXML
    private TextField txtCargo;
    // conexión con el negocio

    private EmpleadoBsn empleadoBsn;

    public AgregarNuevoEmpleadoController() {
        this.empleadoBsn = new EmpleadoBsn();
    }
    //todo actualizar tabla de empleados

    @FXML
    public void initialize() {
        txtGenero.getItems().removeAll(txtGenero.getItems());
        txtGenero.getItems().addAll("Masculino", "Femenino", "Otro", "Prefiero no decirlo");
        txtGenero.setValue(" ");
        txtPreguntaSeguridad.getItems().removeAll(txtPreguntaSeguridad.getItems());
        txtPreguntaSeguridad.getItems().addAll("¿Cual es tu mejor amigo de la infancia?", "¿Cual es tu color favorito?", "¿Cual es tu dia de nacimiento?", "¿Cual es el nombre de tu padre?");
        txtPreguntaSeguridad.setValue(" ");
    }
    public void btnRegresar_action() throws IOException {
        Parent panelModificarEmpleados = FXMLLoader.load(getClass().getResource("../view/modificar-empleados.fxml"));
        this.contenedorAgregarNuevoEmpleado.setCenter(panelModificarEmpleados);
    }

    public void btnGuardar_action() {
        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String nombresYApellidos = txtNombresYApellidos.getText().trim();
        String documento = txtDocumento.getText().trim();
        String telefono = txtTelefono.getText().trim();
        String usuario = txtUsuario.getText().trim();
        String contraseña = txtContraseña.getText().trim();
        String email = txtEmail.getText().trim();
        String edad = txtEdad.getText().trim();
        String genero;
        String preguntaSeguridad;
        String salario = txtSalario.getText().trim();
        String cargo = txtCargo.getText();
        //se valida si el genero no ha sido escogido
        if(txtGenero.getValue().equals(" ")){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El genero es requerido");
            txtGenero.requestFocus();
            return;
        }
        else{
            genero = txtGenero.getValue().toString().trim();
        }
        //se valida si la pregunta de seguridad no ha sido escogida
        if(txtPreguntaSeguridad.getValue().equals(" ")){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La pregunta de seguridad es requerida");
            txtPreguntaSeguridad.requestFocus();
            return;
        }
        else{
            preguntaSeguridad = txtPreguntaSeguridad.getValue().toString().trim();
        }
        String respuestaSeguridad = txtRespuestaSeguridad.getText().trim();

        // se valida que los nombres contenga un valor
        if (nombresYApellidos.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "Los nombres son requeridos");
            txtNombresYApellidos.requestFocus();
            return;
        }


        // se valida que el usuario contenga un valor
        if (usuario.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El usuario es requerido");
            txtUsuario.requestFocus();
            return;
        }

        // se valida que el email contenga un valor
        if (email.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El email es requerido");
            txtEmail.requestFocus();
            return;
        }
        // se valida que la edad contenga un valor
        if (edad.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La edad es requerida");
            txtEdad.requestFocus();
            return;
        }
        // se valida que la respuesta de seguridad contenga un valor
        if (respuestaSeguridad.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La respuesta de seguridad es requerida");
            txtRespuestaSeguridad.requestFocus();
            return;
        }
        // se valida que el salario contenga un valor
        if (salario.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El campo salario es requerido");
            txtSalario.requestFocus();
            return;
        }
        // se valida que el cargo contenga un valor
        if (cargo.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El campo cargo es requerido");
            txtCargo.requestFocus();
            return;
        }

        Empleado empleado = new Empleado(nombresYApellidos, documento, telefono, usuario, contraseña, email, edad, genero, preguntaSeguridad, respuestaSeguridad,salario,cargo);
        try {
            //todo hacer que los métodos de almacenamiento retornen el objeto almacenado (copia)
            this.empleadoBsn.registrarEmpleado(empleado);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro", "Resultado de la transacción", "Se ha sido registrado con éxito");
            limpiarCampos();
            //todo clonar el cliente y agregarlo a la tabla de clientes
            //...
        } catch (EmpleadoYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", e.getMessage());
        }
        //todo tirar excepcion de clonacion no soportada y refrescar tabla
        // catch (CloneNotSupportedException cnse){
        //    this.refrescarTabla();
        //}
    }
    private void limpiarCampos() {
        this.txtNombresYApellidos.clear();
        this.txtDocumento.clear();
        this.txtTelefono.clear();
        this.txtUsuario.clear();
        this.txtContraseña.clear();
        this.txtEmail.clear();
        this.txtEdad.clear();
        this.txtGenero.setValue("");
        this.txtPreguntaSeguridad.setValue("");
        this.txtRespuestaSeguridad.clear();
        this.txtSalario.clear();
        this.txtCargo.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
}
