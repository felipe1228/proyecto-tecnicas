package controller;

import bsn.ClienteBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import model.Cliente;

import java.io.IOException;
import java.util.List;

public class PanelClientesEmpleadoController {
    // conexión con el negocio
    private ClienteBsn clienteBsn;

    public PanelClientesEmpleadoController() {
        this.clienteBsn = new ClienteBsn();
    }
    @FXML
    private BorderPane contenedorClientesEmpleado;
    @FXML
    private TableView<Cliente> tblClientes;
    @FXML
    private TableColumn<Cliente, String> clmUsuario;
    @FXML
    private TableColumn<Cliente, String> clmNombres;
    @FXML
    private TableColumn<Cliente, String> clmDocumento;
    @FXML
    private TableColumn<Cliente, String> clmTelefono;
    @FXML
    private TableColumn<Cliente, String> clmEmail;
    @FXML
    public void initialize(){
        clmUsuario.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUsuario()));
        clmNombres.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombresYApellidos()));
        clmDocumento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDocumento()));
        clmTelefono.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTelefono()));
        clmEmail.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEmail()));
        refrescarTabla();
    }
    public void btnRegresar_action() throws IOException {
        Parent panelEmpleado = FXMLLoader.load(getClass().getResource("../view/panel-empleado.fxml"));
        this.contenedorClientesEmpleado.setCenter(panelEmpleado);
    }
    private void refrescarTabla(){
        List<Cliente> clientesList = clienteBsn.consultarClientes();
        ObservableList<Cliente> clientesListObservable = FXCollections.observableList(clientesList);
        tblClientes.setItems(clientesListObservable);
    }
}
