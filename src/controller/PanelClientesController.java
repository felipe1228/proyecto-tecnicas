package controller;

import bsn.CitaBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import model.Cita;
import model.Cliente;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class PanelClientesController {
    @FXML
    private BorderPane contenedorPanelClientes;
    @FXML
    private Label labelNombres;
    @FXML
    private Label labelDocumento;
    @FXML
    private Label labelEmail;
    @FXML
    private TableView<Cita> tblCitas;
    @FXML
    private TableColumn<Cita, String> clmFecha;
    @FXML
    private TableColumn<Cita, String> clmHora;
    @FXML
    private TableColumn<Cita, String> clmProcedimiento;
    @FXML
    private TableColumn<Cita, String> clmPrecio;

    private CitaBsn citaBsn;
    public PanelClientesController() throws IOException {
        this.citaBsn = new CitaBsn();
    }

    FileReader fr = new FileReader("usuario-logueado");
    BufferedReader br = new BufferedReader(fr);//read 'file'
    String clienteLeido= br.readLine();
    Cliente clienteLogueado = parseClienteObject(clienteLeido);
    public void initialize() {

        labelNombres.setText(clienteLogueado.getNombresYApellidos());
        labelDocumento.setText(clienteLogueado.getDocumento());
        labelEmail.setText(clienteLogueado.getEmail());
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFecha()));
        clmHora.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHora()));
        clmProcedimiento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getServicioRequerido().getNombre()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getServicioRequerido().getPrecio()));
        refrescarTabla();
    }

    public void btnRegresar_action(ActionEvent actionEvent) throws IOException {
        Parent loginPrincipal = FXMLLoader.load(getClass().getResource("../view/login-principal.fxml"));
        this.contenedorPanelClientes.setCenter(loginPrincipal);
    }

    public void btnAgendarNuevaCita_action() throws IOException {
        Parent agendarCitaClientes = FXMLLoader.load(getClass().getResource("../view/agendar-cita-clientes.fxml"));
        this.contenedorPanelClientes.setCenter(agendarCitaClientes);
    }

    public void btnModificarMisDatos_action() throws IOException {
        Parent modificarDatosCliiente = FXMLLoader.load(getClass().getResource("../view/modificar-datos-cliente.fxml"));
        this.contenedorPanelClientes.setCenter(modificarDatosCliiente);
    }
    private Cliente parseClienteObject(String clienteString) {
        String[] datosCliente = clienteString.split(",");
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1], datosCliente[2], datosCliente[3], datosCliente[4], datosCliente[5],datosCliente[6],datosCliente[7],datosCliente[8],datosCliente[9]);
        return cliente;
    }
    private void refrescarTabla(){
        List<Cita> citasList = citaBsn.consultarCitas();
        ObservableList<Cita> citasListObservable = FXCollections.observableList(citasList);
        tblCitas.setItems(citasListObservable);
    }
}
