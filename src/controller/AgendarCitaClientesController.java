package controller;

import bsn.CitaBsn;
import bsn.ServicioBsn;
import bsn.exception.CitaYaExisteException;
import bsn.exception.ServicioYaExisteException;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import model.Cita;
import model.Cliente;
import model.Servicio;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;


public class AgendarCitaClientesController {
    @FXML
    private BorderPane contenedorAgendarCitaClientes;
    @FXML
    private DatePicker txtFecha;
    @FXML
    private ChoiceBox txtHora;
    @FXML
    private ChoiceBox txtServicios;


    private CitaBsn citaBsn;

    FileReader fr = new FileReader("usuario-logueado");
    BufferedReader br = new BufferedReader(fr);//read 'file'
    String clienteLeido= br.readLine();
    Cliente clienteLogueado = parseClienteObject(clienteLeido);

    FileReader readerServicios = new FileReader("servicios-v2");
    BufferedReader bufferServicios = new BufferedReader(readerServicios);//read 'file'
    String servicioLeido = bufferServicios.readLine();

    @FXML
    public void initialize() throws IOException {
        txtFecha.setValue(LocalDate.now());
        txtHora.getItems().removeAll(txtHora.getItems());
        txtHora.getItems().addAll("10:00 AM", "10:30 AM", "11:00 AM", "11:30 AM", "12:00 M", "12:30 PM","01:00 PM", "1:30 PM", "2:00 PM",  "2:30 PM", "3:00 PM", "3:30 PM", "4:00 PM", "4:30 PM", "5:00 PM", "5:30 PM", "6:00 PM");
        txtHora.setValue(" ");
        txtServicios.getItems().removeAll(txtServicios.getItems());
        do{
            txtServicios.getItems().add(parseServicioObject(servicioLeido).getNombre());
            servicioLeido = bufferServicios.readLine();
        }
        while(bufferServicios.readLine()!=null);

    }
    public AgendarCitaClientesController() throws IOException {
        this.citaBsn = new CitaBsn();
    }
    //todo actualizar tabla de empleados

    public void btnRegresar_action() throws IOException {
        Parent panelClientes = FXMLLoader.load(getClass().getResource("../view/panel-clientes.fxml"));
        this.contenedorAgendarCitaClientes.setCenter(panelClientes);
    }

    public void btnAgendar_action() throws IOException {
        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String fecha = txtFecha.getValue().toString().trim();
        String hora = txtHora.getValue().toString().trim();
        String servicio = retornarServicioCompleto(txtServicios.getValue().toString().trim());


        // se valida que la fecha contenga un valor
        if (fecha.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La fecha es requerida");
            txtFecha.requestFocus();
            return;
        }


        // se valida que la hora contenga un valor
        if (hora.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La hora es requerida");
            txtHora.requestFocus();
            return;
        }
        // se valida que el servicio contenga un valor
        if (servicio.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El servicio es requerido");
            txtServicios.requestFocus();
            return;
        }



        Cita cita = new Cita(clienteLogueado, fecha, hora, parseServicioObject(servicio));
        try {
            //todo hacer que los métodos de almacenamiento retornen el objeto almacenado (copia)
            this.citaBsn.registrarCita(cita);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro", "Resultado de la transacción", "Se ha sido registrado con éxito");
            limpiarCampos();
            //todo clonar el cliente y agregarlo a la tabla de clientes
            //...
        } catch (CitaYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", e.getMessage());
        }
        //todo tirar excepcion de clonacion no soportada y refrescar tabla
        // catch (CloneNotSupportedException cnse){
        //    this.refrescarTabla();
        //}
    }
    private void limpiarCampos() {
        this.txtServicios.setValue("");
        this.txtFecha.setValue(LocalDate.now());
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
    private Cliente parseClienteObject(String clienteString) {
        String[] datosCliente = clienteString.split(",");
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1], datosCliente[2], datosCliente[3], datosCliente[4], datosCliente[5],datosCliente[6],datosCliente[7],datosCliente[8],datosCliente[9]);
        return cliente;
    }
    private Servicio parseServicioObject(String servicioString) {
        String[] datosServicio = servicioString.split(",");
        Servicio servicio = new Servicio(datosServicio[0], datosServicio[1], datosServicio[2]);
        return servicio;
    }
    private String retornarServicioCompleto(String servicio) throws IOException {
        FileReader readerServicios = new FileReader("servicios-v2");
        BufferedReader bufferServicios = new BufferedReader(readerServicios);//read 'file'
        String servicioLeido = bufferServicios.readLine();
        String retornarServicio="";
        do{
            if(servicioLeido.contains(servicio)){
                retornarServicio = servicioLeido;
            }
            servicioLeido = bufferServicios.readLine();
        }
        while(bufferServicios.readLine()!=null);
        return retornarServicio;
    }

}
