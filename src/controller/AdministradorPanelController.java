package controller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.Label;
import model.Empleado;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AdministradorPanelController {
    @FXML
    private BorderPane contenedorAdministradorPanel;
    @FXML
    private Label lblBienvenido;

    FileReader fr;

    {
        try {
            fr = new FileReader("usuario-logueado");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    BufferedReader br = new BufferedReader(fr);//read 'file'
    String empleadoLeido;

    {
        try {
            empleadoLeido = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Empleado empleadoLogueado = parseEmpleadoObject(empleadoLeido);

    public void initialize(){
        lblBienvenido.setText("Bienvenido, administrador "+empleadoLogueado.getNombresYApellidos()+"!");
    }
    public void btnModificarEmpleados_action() throws IOException {
        Parent modificarEmpleados = FXMLLoader.load(getClass().getResource("../view/modificar-empleados.fxml"));
        this.contenedorAdministradorPanel.setCenter(modificarEmpleados);
    }

    public void btnModificarServicios_action() throws IOException {
        Parent modificarServicios = FXMLLoader.load(getClass().getResource("../view/modificar-servicios.fxml"));
        this.contenedorAdministradorPanel.setCenter(modificarServicios);
    }

    public void btnRegresar_action() throws IOException {
        Parent loginPrincipal = FXMLLoader.load(getClass().getResource("../view/login-principal.fxml"));
        this.contenedorAdministradorPanel.setCenter(loginPrincipal);
    }
    private Empleado parseEmpleadoObject(String empleadoString) {
        String[] datosEmpleado = empleadoString.split(",");
        Empleado empleado = new Empleado(datosEmpleado[0], datosEmpleado[1], datosEmpleado[2], datosEmpleado[3], datosEmpleado[4], datosEmpleado[5],datosEmpleado[6],datosEmpleado[7],datosEmpleado[8],datosEmpleado[9],datosEmpleado[10],datosEmpleado[11]);
        return empleado;
    }
}
