package controller;

import bsn.ClienteBsn;
import bsn.exception.ClienteYaExisteException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import model.Cliente;
import java.io.IOException;

public class RegistrarCuentaController {
    @FXML
    private BorderPane contenedorRegistro;
    @FXML
    private TextField txtNombresYApellidos;
    @FXML
    private TextField txtDocumento;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField txtContraseña;
    @FXML
    private PasswordField txtRepetirContraseña;
    @FXML
    private TextField txtEmail;
    @FXML
    private TextField txtEdad;
    @FXML
    private ChoiceBox txtGenero;
    @FXML
    private ChoiceBox txtPreguntaSeguridad;
    @FXML
    private TextField txtRespuestaSeguridad;

    @FXML
    public void initialize() {
        txtGenero.getItems().removeAll(txtGenero.getItems());
        txtGenero.getItems().addAll("Masculino", "Femenino", "Otro", "Prefiero no decirlo");
        txtGenero.setValue(" ");
        txtPreguntaSeguridad.getItems().removeAll(txtPreguntaSeguridad.getItems());
        txtPreguntaSeguridad.getItems().addAll("¿Cual es tu mejor amigo de la infancia?", "¿Cual es tu color favorito?", "¿Cual es tu dia de nacimiento?", "¿Cual es el nombre de tu padre?");
        txtPreguntaSeguridad.setValue(" ");
    }
    // conexión con el negocio

    private ClienteBsn clienteBsn;

    public RegistrarCuentaController() {
        this.clienteBsn = new ClienteBsn();
    }
    //todo actualizar tabla de clientes

    public void btnRegresar_action() throws IOException {
        Parent loginPrincipal = FXMLLoader.load(getClass().getResource("../view/login-principal.fxml"));
        this.contenedorRegistro.setCenter(loginPrincipal);
    }

    public void btnRegistrar_action() {
        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String nombresYApellidos = txtNombresYApellidos.getText().trim();
        String documento = txtDocumento.getText().trim();
        String telefono = txtTelefono.getText().trim();
        String usuario = txtUsuario.getText().trim();
        String contraseña = txtContraseña.getText().trim();
        String repetirContraseña = txtRepetirContraseña.getText().trim();
        String email = txtEmail.getText().trim();
        String edad = txtEdad.getText().trim();
        String genero;
        String preguntaSeguridad;
        //se valida si el genero no ha sido escogido
        if(txtGenero.getValue().equals(" ")){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El genero es requerido");
            txtGenero.requestFocus();
            return;
        }
        else{
            genero = txtGenero.getValue().toString().trim();
        }
        //se valida si la pregunta de seguridad no ha sido escogida
        if(txtPreguntaSeguridad.getValue().equals(" ")){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La pregunta de seguridad es requerida");
            txtPreguntaSeguridad.requestFocus();
            return;
        }
        else{
            preguntaSeguridad = txtPreguntaSeguridad.getValue().toString().trim();
        }
        String respuestaSeguridad = txtRespuestaSeguridad.getText().trim();

        // se valida que los nombres contenga un valor
        if (nombresYApellidos.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "Los nombres son requeridos");
            txtNombresYApellidos.requestFocus();
            return;
        }


        // se valida que el usuario contenga un valor
        if (usuario.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El usuario es requerido");
            txtUsuario.requestFocus();
            return;
        }

        // se valida que la contraseña contenga un valor
        if (repetirContraseña.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El campo repetir contraseña es requerido");
            txtRepetirContraseña.requestFocus();
            return;
        }
        // se valida si los campos de contraseña y repetir contraseña son iguales
        if (!contraseña.equals(repetirContraseña)) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "Los campos contraseña y repetir contraseña no coinciden");
            txtRepetirContraseña.requestFocus();
            return;
        }

        // se valida que el email contenga un valor
        if (email.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "El email es requerido");
            txtEmail.requestFocus();
            return;
        }
        // se valida que la edad contenga un valor
        if (edad.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La edad es requerida");
            txtEdad.requestFocus();
            return;
        }
        // se valida que la respuesta de seguridad contenga un valor
        if (respuestaSeguridad.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", "La respuesta de seguridad es requerida");
            txtRespuestaSeguridad.requestFocus();
            return;
        }

        Cliente cliente = new Cliente(nombresYApellidos, documento, telefono, usuario, contraseña, email, edad, genero, preguntaSeguridad, respuestaSeguridad);
        try {
            //todo hacer que los métodos de almacenamiento retornen el objeto almacenado (copia)
            this.clienteBsn.registrarCliente(cliente);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro", "Resultado de la transacción", "Se ha sido registrado con éxito");
            limpiarCampos();
            //todo clonar el cliente y agregarlo a la tabla de clientes
            //...
        } catch (ClienteYaExisteException e) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro", "Resultado de la transacción", e.getMessage());
        }
        //todo tirar excepcion de clonacion no soportada y refrescar tabla
        // catch (CloneNotSupportedException cnse){
        //    this.refrescarTabla();
        //}
    }

    private void limpiarCampos() {
        this.txtNombresYApellidos.clear();
        this.txtDocumento.clear();
        this.txtTelefono.clear();
        this.txtUsuario.clear();
        this.txtContraseña.clear();
        this.txtRepetirContraseña.clear();
        this.txtEmail.clear();
        this.txtEdad.clear();
        this.txtGenero.setValue("");
        this.txtPreguntaSeguridad.setValue("");
        this.txtRespuestaSeguridad.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
}
