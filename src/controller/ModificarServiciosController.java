package controller;

import bsn.EmpleadoBsn;
import bsn.ServicioBsn;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import model.Servicio;

import java.io.IOException;
import java.util.List;

public class ModificarServiciosController {
    @FXML
    private BorderPane contenedorModificarServicios;
    @FXML
    private TableView<Servicio> tblServicios;
    @FXML
    private TableColumn<Servicio, String> clmNombre;
    @FXML
    private TableColumn<Servicio, String> clmDescripcion;
    @FXML
    private TableColumn<Servicio, String> clmPrecio;

    // conexión con el negocio
    private ServicioBsn servicioBsn;

    public ModificarServiciosController() {
        this.servicioBsn = new ServicioBsn();
    }

    @FXML
    public void initialize(){
        clmNombre.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNombre()));
        clmDescripcion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDescripcion()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecio()));
        refrescarTabla();
    }
    public void btnRegresar_action() throws IOException {
        Parent ventanaAdministradorPanel = FXMLLoader.load(getClass().getResource("../view/administrador-panel.fxml"));
        this.contenedorModificarServicios.setCenter(ventanaAdministradorPanel);
    }

    public void btnAgregarNuevoServicio_action() throws IOException {
        Parent ventanaAgregarNuevoServicio = FXMLLoader.load(getClass().getResource("../view/agregar-nuevo-servicio.fxml"));
        this.contenedorModificarServicios.setCenter(ventanaAgregarNuevoServicio);
    }
    private void refrescarTabla(){
        List<Servicio> serviciosList = servicioBsn.consultarServicios();
        ObservableList<Servicio> serviciosListObservable = FXCollections.observableList(serviciosList);
        tblServicios.setItems(serviciosListObservable);
    }
}
