package dao;

import model.Cliente;

import java.util.List;
import java.util.Optional;

public interface ClienteDAO {
    void registrarCliente(Cliente cliente);
    List<Cliente> consultarClientes();
    Optional<Cliente> consultarPorDocumento(String documento);
}
