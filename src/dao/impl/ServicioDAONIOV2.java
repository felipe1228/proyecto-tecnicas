package dao.impl;

import dao.ServicioDAO;
import model.Servicio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class ServicioDAONIOV2 implements ServicioDAO {
    private final static String NOMBRE_ARCHIVO = "servicios-v2";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public ServicioDAONIOV2() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    @Override
    public void registrarServicio(Servicio servicio) {
        String registro = parseServicioString(servicio);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    private String parseServicioString(Servicio servicio) {
        StringBuilder sb = new StringBuilder();
        sb.append(servicio.getNombre()).append(FIELD_SEPARATOR)
                .append(servicio.getDescripcion()).append(FIELD_SEPARATOR)
                .append(servicio.getPrecio()).append(RECORD_SEPARATOR);
        return sb.toString();
    }
    @Override
    public Optional<Servicio> consultarPorNombre(String nombre) {
        return Optional.empty();
    }

    @Override
    public List<Servicio> consultarServicios() {
        List<Servicio> servicios = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(servicioString -> {
                Servicio servicioConvertido = parseServicioObject(servicioString);
                servicios.add(servicioConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return servicios;
    }
    private Servicio parseServicioObject(String servicioString) {
        String[] datosServicio = servicioString.split(FIELD_SEPARATOR);
        Servicio servicio = new Servicio(datosServicio[0], datosServicio[1], datosServicio[2]);
        return servicio;
    }

}