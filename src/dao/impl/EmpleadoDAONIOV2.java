package dao.impl;

import dao.EmpleadoDAO;
import model.Empleado;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class EmpleadoDAONIOV2 implements EmpleadoDAO {
    private final static String NOMBRE_ARCHIVO = "empleados-v2";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String NOMBRE_ARCHIVO_BORRAR = "empleados-v2-temporal";
    private final static Path ARCHIVO_TEMPORAL = Paths.get(NOMBRE_ARCHIVO_BORRAR);
    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public EmpleadoDAONIOV2() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    @Override
    public void registrarEmpleado(Empleado empleado) {
        String registro = parseEmpleadoString(empleado);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    private String parseEmpleadoString(Empleado empleado) {
        StringBuilder sb = new StringBuilder();
        sb.append(empleado.getNombresYApellidos()).append(FIELD_SEPARATOR)
                .append(empleado.getDocumento()).append(FIELD_SEPARATOR)
                .append(empleado.getTelefono()).append(FIELD_SEPARATOR)
                .append(empleado.getUsuario()).append(FIELD_SEPARATOR)
                .append(empleado.getContraseña()).append(FIELD_SEPARATOR)
                .append(empleado.getEmail()).append(FIELD_SEPARATOR)
                .append(empleado.getEdad()).append(FIELD_SEPARATOR)
                .append(empleado.getGenero()).append(FIELD_SEPARATOR)
                .append(empleado.getPreguntaSeguridad()).append(FIELD_SEPARATOR)
                .append(empleado.getRespuestaSeguridad()).append(FIELD_SEPARATOR)
                .append(empleado.getSalario()).append(FIELD_SEPARATOR)
                .append(empleado.getCargo()).append(RECORD_SEPARATOR);
        return sb.toString();
    }
    @Override
    public Optional<Empleado> consultarPorDocumento(String documento) {
        return Optional.empty();
    }


    @Override
    public List<Empleado> consultarEmpleados() {
        List<Empleado> empleados = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> {
                Empleado empleadoConvertido = parseEmpleadoObject(clienteString);
                empleados.add(empleadoConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return empleados;
    }
    private Empleado parseEmpleadoObject(String empleadoString) {
        String[] datosEmpleado = empleadoString.split(FIELD_SEPARATOR);
        Empleado empleado = new Empleado(datosEmpleado[0], datosEmpleado[1], datosEmpleado[2], datosEmpleado[3], datosEmpleado[4], datosEmpleado[5],datosEmpleado[6],datosEmpleado[7],datosEmpleado[8],datosEmpleado[9],datosEmpleado[10],datosEmpleado[11]);
        return empleado;
    }

}
