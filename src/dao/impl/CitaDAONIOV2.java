package dao.impl;

import dao.CitaDAO;
import model.Cita;
import model.Cliente;
import model.Servicio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class CitaDAONIOV2 implements CitaDAO {
    private final static String NOMBRE_ARCHIVO = "citas-v2";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private static final String FIELD_SEPARATOR = ",";
    private static final String CITAS_FIELD_SEPARATOR = ";";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public CitaDAONIOV2() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
    @Override
    public void registrarCita(Cita cita) {
        String registro = parseCitaString(cita);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
    private String parseCitaString(Cita cita) {
        StringBuilder sb = new StringBuilder();
        sb.append(cita.getClienteSolicitante()).append(CITAS_FIELD_SEPARATOR)
                .append(cita.getFecha()).append(CITAS_FIELD_SEPARATOR)
                .append(cita.getHora()).append(CITAS_FIELD_SEPARATOR)
                .append(cita.getServicioRequerido()).append(RECORD_SEPARATOR);
        return sb.toString();
    }
    @Override
    public Optional<Cita> consultarPorFecha(String nombre) {
        return Optional.empty();
    }

    @Override
    public List<Cita> consultarCitas() {
        List<Cita> citas = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(citaString -> {
                Cita citaConvertido = parseCitaObject(citaString);
                citas.add(citaConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return citas;
    }
    private Cita parseCitaObject(String citaString) {
        String[] datosCita = citaString.split(CITAS_FIELD_SEPARATOR);
        Cita cita = new Cita(parseClienteObject(datosCita[0]), datosCita[1], datosCita[2],parseServicioObject(datosCita[3]));
        return cita;
    }
    private Cliente parseClienteObject(String clienteString) {
        String nuevoString = clienteString.substring(8,clienteString.length()-1);
        nuevoString = nuevoString.replace("'","");
        nuevoString = nuevoString.replace("nombresYApellidos=","");
        nuevoString = nuevoString.replace(" documento=","");
        nuevoString = nuevoString.replace(" telefono=","");
        nuevoString = nuevoString.replace(" usuario=","");
        nuevoString = nuevoString.replace(" contraseña=","");
        nuevoString = nuevoString.replace(" email=","");
        nuevoString = nuevoString.replace(" edad=","");
        nuevoString = nuevoString.replace(" genero=","");
        nuevoString = nuevoString.replace(" preguntaSeguridad=","");
        nuevoString = nuevoString.replace(" respuestaSeguridad=","");
        String[] datosCliente = nuevoString.split(FIELD_SEPARATOR);
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1], datosCliente[2], datosCliente[3], datosCliente[4], datosCliente[5],datosCliente[6],datosCliente[7],datosCliente[8],datosCliente[9]);
        return cliente;
    }
    private Servicio parseServicioObject(String servicioString) {
        String nuevoString = servicioString.substring(9,servicioString.length()-1);
        nuevoString = nuevoString.replace("'","");
        nuevoString = nuevoString.replace("nombre=","");
        nuevoString = nuevoString.replace(" descripcion=","");
        nuevoString = nuevoString.replace(" precio=","");
        String[] datosServicio = nuevoString.split(FIELD_SEPARATOR);
        Servicio servicio = new Servicio(datosServicio[0], datosServicio[1], datosServicio[2]);
        return servicio;
    }
}