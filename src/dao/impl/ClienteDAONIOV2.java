package dao.impl;

import dao.ClienteDAO;
import model.Cliente;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class ClienteDAONIOV2 implements ClienteDAO {
    private final static String NOMBRE_ARCHIVO = "clientes-v2";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private static final String FIELD_SEPARATOR = ",";
    private static final String RECORD_SEPARATOR = System.lineSeparator();

    public ClienteDAONIOV2() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarCliente(Cliente cliente) {
        String registro = parseClienteString(cliente);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    private String parseClienteString(Cliente cliente) {
        StringBuilder sb = new StringBuilder();
        sb.append(cliente.getNombresYApellidos()).append(FIELD_SEPARATOR)
                .append(cliente.getDocumento()).append(FIELD_SEPARATOR)
                .append(cliente.getTelefono()).append(FIELD_SEPARATOR)
                .append(cliente.getUsuario()).append(FIELD_SEPARATOR)
                .append(cliente.getContraseña()).append(FIELD_SEPARATOR)
                .append(cliente.getEmail()).append(FIELD_SEPARATOR)
                .append(cliente.getEdad()).append(FIELD_SEPARATOR)
                .append(cliente.getGenero()).append(FIELD_SEPARATOR)
                .append(cliente.getPreguntaSeguridad()).append(FIELD_SEPARATOR)
                .append(cliente.getRespuestaSeguridad()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public Optional<Cliente> consultarPorDocumento(String documento) {
        return Optional.empty();
    }

    @Override
    public List<Cliente> consultarClientes() {
        List<Cliente> clientes = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> {
                Cliente clienteConvertido = parseClienteObject(clienteString);
                clientes.add(clienteConvertido);
            });
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return clientes;
    }

    private Cliente parseClienteObject(String clienteString) {
        String[] datosCliente = clienteString.split(FIELD_SEPARATOR);
        Cliente cliente = new Cliente(datosCliente[0], datosCliente[1], datosCliente[2], datosCliente[3], datosCliente[4], datosCliente[5],datosCliente[6],datosCliente[7],datosCliente[8],datosCliente[9]);
        return cliente;
    }

}
