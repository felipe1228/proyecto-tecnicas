package dao;

import model.Cita;

import java.util.List;
import java.util.Optional;

public interface CitaDAO {
    void registrarCita(Cita cita);
    List<Cita> consultarCitas();
    Optional<Cita> consultarPorFecha(String hora);
}

