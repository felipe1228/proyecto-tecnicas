package dao;

import model.Empleado;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface EmpleadoDAO {
    void registrarEmpleado(Empleado empleado);
    List<Empleado> consultarEmpleados();
    Optional<Empleado> consultarPorDocumento(String documento);
}
