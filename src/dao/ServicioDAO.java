package dao;

import model.Servicio;

import java.util.List;
import java.util.Optional;

public interface ServicioDAO {
    void registrarServicio(Servicio servicio);
    List<Servicio> consultarServicios();
    Optional<Servicio> consultarPorNombre(String nombre);
}
