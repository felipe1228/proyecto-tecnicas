package bsn;

import bsn.exception.ClienteYaExisteException;
import bsn.exception.EmpleadoYaExisteException;
import dao.EmpleadoDAO;
import dao.impl.EmpleadoDAONIOV2;
import model.Empleado;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class EmpleadoBsn {
    private EmpleadoDAO empleadoDAO;
    public EmpleadoBsn() {
        this.empleadoDAO = new EmpleadoDAONIOV2();
    }

    public void registrarEmpleado(Empleado empleado) throws EmpleadoYaExisteException {
        Optional<Empleado> empleadoOptional = this.empleadoDAO.consultarPorDocumento(empleado.getDocumento());
        // el empleado no estaba
        if (empleadoOptional.isPresent()) {
            throw new EmpleadoYaExisteException();
        } else {
            this.empleadoDAO.registrarEmpleado(empleado);
        }
    }

    public List<Empleado> consultarEmpleados() {
        return empleadoDAO.consultarEmpleados();
    }
}
