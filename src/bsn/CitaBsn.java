package bsn;

import bsn.exception.CitaYaExisteException;
import dao.CitaDAO;
import dao.impl.CitaDAONIOV2;
import model.Cita;
import model.Cliente;

import java.util.List;
import java.util.Optional;

public class CitaBsn {

    private CitaDAO citaDAO;
    public CitaBsn() {
        this.citaDAO = new CitaDAONIOV2();
    }
    public void registrarCita(Cita cita) throws CitaYaExisteException {
        Optional<Cita> citaOptional = this.citaDAO.consultarPorFecha(cita.getFecha());
        // la cita no existe
        if (citaOptional.isPresent()) {
            throw new CitaYaExisteException();
        } else {
            this.citaDAO.registrarCita(cita);
        }
    }

    public List<Cita> consultarCitas() {
        return citaDAO.consultarCitas();
    }
}
