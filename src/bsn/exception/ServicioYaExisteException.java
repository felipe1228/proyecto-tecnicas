package bsn.exception;

public class ServicioYaExisteException extends Exception{
    public ServicioYaExisteException() {
        super("Ya existe un servicio con el nombre ingresado");
    }
}
