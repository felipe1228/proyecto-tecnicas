package bsn.exception;

public class CitaYaExisteException extends Exception{
    public CitaYaExisteException() {
        super("El la fecha para su cita ya esta ocupada");
    }
}
