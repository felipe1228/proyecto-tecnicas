package bsn;

import bsn.exception.ServicioYaExisteException;
import dao.ServicioDAO;
import dao.impl.ServicioDAONIOV2;
import model.Servicio;

import java.util.List;
import java.util.Optional;

public class ServicioBsn {
    private ServicioDAO servicioDAO;
    public ServicioBsn() {
        this.servicioDAO = new ServicioDAONIOV2();
    }

    public void registrarServicio(Servicio servicio) throws ServicioYaExisteException {
        Optional<Servicio> servicioOptional = this.servicioDAO.consultarPorNombre(servicio.getNombre());
        // el empleado no estaba
        if (servicioOptional.isPresent()) {
            throw new ServicioYaExisteException();
        } else {
            this.servicioDAO.registrarServicio(servicio);
        }
    }
    public List<Servicio> consultarServicios() {
        return servicioDAO.consultarServicios();
    }
}
