package bsn;

import bsn.exception.ClienteYaExisteException;
import dao.ClienteDAO;
import dao.impl.ClienteDAONIOV2;
import model.Cliente;

import java.util.List;
import java.util.Optional;

public class ClienteBsn {

    private ClienteDAO clienteDAO;
    public ClienteBsn() {
        this.clienteDAO = new ClienteDAONIOV2();
    }

    public void registrarCliente(Cliente cliente) throws ClienteYaExisteException {
        Optional<Cliente> clienteOptional = this.clienteDAO.consultarPorDocumento(cliente.getDocumento());
        // el cliente no estaba
        if (clienteOptional.isPresent()) {
            throw new ClienteYaExisteException();
        } else {
            this.clienteDAO.registrarCliente(cliente);
        }
    }

    public List<Cliente> consultarClientes() {
        return clienteDAO.consultarClientes();
    }
}
